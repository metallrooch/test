import pandas as pd
import json

df = pd.read_csv('eng_fr.csv', header=0, index_col=False)
items_number = len(df['Title'].unique())
per_title = df['Title'].count() / len(df['Title']) * 100
per_description = df['Description'].count() / len(df['Description']) * 100


def save_json(data):
    with open('result.json', 'w') as f:
        json.dump(data, f)

data = {
    'Per_title': per_title,
    'Per_description': per_description,
    'General items': items_number
}

save_json(data)
