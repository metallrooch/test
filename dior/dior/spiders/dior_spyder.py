# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import Request
import re
import time


class DiorSpider(scrapy.Spider):
    name = 'dior'
    allowed_domains = ['dior.com']
    # start_urls = ['http://dior.com/en_us']

    def __init__(self, eng_fr=None, all=None, other=None):
        self.all = all
        self.eng_fr = eng_fr
        self.other = other

    def start_requests(self):
        url_en = 'https://www.dior.com/en_us/'
        url_fr = 'https://www.dior.com/fr_fr/'

        if self.eng_fr:
            yield Request(url=url_en, callback=self.parse)
            yield Request(url=url_fr, callback=self.parse)
        else:
            yield Request(url=url_en, callback=self.parse_all)
        
    def parse_all(self, response):
        if self.other:
            urls = response.xpath('//*[@class="locale-row"]')
            for url in urls:
                title = url.xpath('.//span[@class="locale-row-title"]/text()').extract_first()
                title = re.search(r'[\w\s]+', title).group().strip()
                if title == self.other:
                    url = url.xpath('.//@href').extract_first()
                    url_absolute = response.urljoin(url)
                    yield Request(url=url_absolute, callback=self.parse)
        
        if self.all:
            urls = response.xpath('//*[@class="locale-row"]')
            for url in urls:
                title = url.xpath('.//span[@class="locale-row-title"]/text()').extract_first()
                title = re.search(r'[\w\s]+', title).group().strip()
                url = url.xpath('.//@href').extract_first()
                url_absolute = response.urljoin(url)
                yield Request(url=url_absolute, callback=self.parse)

    def parse(self, response):
        #nav_items = response.xpath('//*[@id="nav"]/div[@class="navigation-items"]/ul/li')
        nav_items = response.xpath('//li[@class="navigation-item"]')
        
        for nav_item in nav_items:
            
            urls = nav_item.xpath('.//ul/li/a/@href').extract()
            for url in urls:
                url_absolute = response.urljoin(url)
                yield Request(url_absolute, callback=self.parse_items)

    def parse_items(self, response):
        item_links = response.xpath('//*[@class="product-link"]/@href').extract()
        for item_link in item_links:
            item_link_absolute = response.urljoin(item_link)
            t1 = time.time()
            yield Request(item_link_absolute, callback=self.parse_items)

            item_title = response.xpath('//*[@class="product-titles"]/h1/span/text()').extract_first()
            item_title = item_title.strip() if item_title else ''
            price = response.xpath('//*[@class="price-line"]/text()').extract_first()
            currency = re.search(r'[€$]', price).group()
            item_price = float(re.search(r'\d+', price).group())
            item_currency = 'EUR' if currency == '€' else 'USD'
            item_description = response.xpath('//*[@id="the-description-tab-content"]/div/div/div/text()').extract()
            item_description = " ".join(item_description).strip()
            item_region = response.xpath('//*[@id="country-select-drop-down-button"]/span/span[2]/text()').extract_first()
            #item_region = re.search(r'[\w\s]+', item_region).group().strip()
            size = response.xpath('//*[@class="size variation is-available"]/text()')
            t2 = time.time()

            d = {
                'Title': item_title,
                'Price': item_price,
                'Currency': item_currency,
                'Description': item_description,
                'Region': item_region,
                'Time': t2 - t1,
            }

            yield d


   